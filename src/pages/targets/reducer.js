import {combineReducers} from 'redux';

import {
    TARGETS_FETCH,
    TARGET_UPDATE,
    TARGET_DELETE,
    TARGET_CREATE,

    targetsFetchLoadingReducer,
    targetsCreateLoadingReducer,
    targetsUpdateLoadingReducer,
    targetsDeleteLoadingReducer
} from './actions';

function targetsReducer(state = [], {type, payload}) {
    switch (type) {
        case TARGETS_FETCH:
            return payload.data;
        case TARGET_UPDATE:
            return state.map(item => item.targets_id !== payload.data.targets_id ? item : payload.data);
        case TARGET_DELETE:
            return state.filter(item => item.targets_id !== payload.data.targets_id);
        case TARGET_CREATE:
            return [...state, payload.data];
        default: return state
    }
}

export const reducer = combineReducers({
    targets: targetsReducer,
    targetsFetchLoading: targetsFetchLoadingReducer,
    targetsCreateLoading: targetsCreateLoadingReducer,
    targetsUpdateLoading: targetsUpdateLoadingReducer,
    targetsDeleteLoading: targetsDeleteLoadingReducer
});

export const targetsSelector = state => state.targets.targets;
export const targetsFetchLoadingSelector = state => state.targets.targetsFetchLoading.isLoading;
export const targetsUpdateLoadingSelector = state => state.targets.targetsUpdateLoading.isLoading;