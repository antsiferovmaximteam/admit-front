export {
    TargetsView,
    TargetsPage
} from './components/targets';
export {
    reducer as targetsReducer,
    targetsSelector,
    targetsFetchLoadingSelector,
    targetsUpdateLoadingSelector
} from './reducer';
export {
    fetchTargets,
    createTargets,
    deleteTargets,
    updateTargets
} from './actions';