import React from 'react';

import {Container, Section} from 'ui';
import {TargetsTable} from '../organisms';

export const TargetsView = () => (
    <Section>
        <Container>
            <TargetsTable/>
        </Container>
    </Section>
);

export const TargetsPage = TargetsView;