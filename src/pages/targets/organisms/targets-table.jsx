import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    targetsSelector,
    targetsFetchLoadingSelector
} from '../reducer';
import {fetchTargets, updateTargets, createTargets, deleteTargets} from '../actions';
import {RestTable} from 'ui';

const mapStateTpProps = (state) => ({
    targets: targetsSelector(state),
    targetsLoading: targetsFetchLoadingSelector(state),
});

const mapDispatchToProps = {
    fetchTargets, updateTargets, createTargets, deleteTargets
};

export class TargetsTableView extends Component {
    constructor(props) {
        super(props);

        this.config = [
            {key: 'targets_id', width: '20%', name: 'Target Id', change: false},
            {key: 'name', width: '60%'},
        ]
    }

    componentDidMount() {
        this.props.fetchTargets();
    }


    onDeleteTarget = (targets) => this.props.deleteExchange(targets.targets_id);

    onUpdateTarget = (targets) => this.props.updateExchange(targets, targets.targets_id);

    render() {
        const {
            targets,
            targetsLoading,
            createTargets
        } = this.props;

        if (targetsLoading || !targets) {
            return <div/>
        }

        return (
            <RestTable
                items={targets}
                config={this.config}
                updateItem={this.onUpdateTarget}
                createItem={createTargets}
                deleteItem={this.onDeleteTarget}
            />
        );
    }
}

export const TargetsTable = connect(mapStateTpProps, mapDispatchToProps)(TargetsTableView);