import {makeLoadingReducer, CRUDAction} from "lib/redux";
import {CRUDApi} from 'lib/net';

export const TARGETS_FETCH = 'TARGETS_FETCH';
export const TARGET_UPDATE = 'TARGET_UPDATE';
export const TARGET_DELETE = 'TARGET_DELETE';
export const TARGET_CREATE = 'TARGET_CREATE';

const Api = new CRUDApi('/targets');

export const {actions: targetsFetchLoading, reducer: targetsFetchLoadingReducer} = makeLoadingReducer(TARGETS_FETCH);
export const {actions: targetsUpdateLoading, reducer: targetsUpdateLoadingReducer} = makeLoadingReducer(TARGET_UPDATE);
export const {actions: targetsDeleteLoading, reducer: targetsDeleteLoadingReducer} = makeLoadingReducer(TARGET_DELETE);
export const {actions: targetsCreateLoading, reducer: targetsCreateLoadingReducer} = makeLoadingReducer(TARGET_CREATE);

export const fetchTargets = CRUDAction(Api.fetch, targetsFetchLoading);
export const updateTargets = CRUDAction(Api.update, targetsUpdateLoading);
export const deleteTargets = CRUDAction(Api.delete, targetsDeleteLoading);
export const createTargets = CRUDAction(Api.create, targetsCreateLoading);