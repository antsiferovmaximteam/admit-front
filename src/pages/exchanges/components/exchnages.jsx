import React from 'react';

import {Container, Section} from 'ui';
import {ExchangesTable} from '../organisms';

export const ExchangesView = () => (
    <Section>
        <Container>
            <ExchangesTable/>
        </Container>
    </Section>
);


export const ExchangesPage = ExchangesView;