import {makeLoadingReducer, CRUDAction} from "lib/redux";
import {CRUDApi} from 'lib/net';

export const EXCHANGES_FETCH = 'EXCHANGES_FETCH';
export const EXCHANGE_UPDATE = 'EXCHANGE_UPDATE';
export const EXCHANGE_DELETE = 'EXCHANGE_DELETE';
export const EXCHANGES_CREATE = 'EXCHANGES_CREATE';

export const {actions: exchangesFetchLoading, reducer: exchangesFetchLoadingReducer} = makeLoadingReducer(EXCHANGES_FETCH);
export const {actions: exchangeUpdateLoading, reducer: exchangeUpdateLoadingReducer} = makeLoadingReducer(EXCHANGE_UPDATE);
export const {actions: exchangeDeleteLoading, reducer: exchangeDeleteLoadingReducer} = makeLoadingReducer(EXCHANGE_DELETE);
export const {actions: exchangeCreateLoading, reducer: exchangeCreateLoadingReducer} = makeLoadingReducer(EXCHANGES_CREATE);

const Api = new CRUDApi('/exchanges');

export const fetchExchanges = CRUDAction(Api.fetch, exchangesFetchLoading);
export const updateExchange = CRUDAction(Api.update, exchangeUpdateLoading, true);
export const deleteExchange = CRUDAction(Api.delete, exchangeDeleteLoading, true);
export const createExchange = CRUDAction(Api.create, exchangeCreateLoading, true);
