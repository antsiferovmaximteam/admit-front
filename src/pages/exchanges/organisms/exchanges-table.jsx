import React, { Component } from 'react';
import {connect} from 'react-redux';

import {
    exchangesSelector,
    exchangesFetchLoadingSelector,
    exchangeUpdateLoadingSelector,
} from '../reducer';
import {fetchExchanges, updateExchange, deleteExchange, createExchange} from '../actions';
import {RestTable} from 'ui';

const mapStateTpProps = (state) => ({
    exchanges: exchangesSelector(state),
    exchangesLoading: exchangesFetchLoadingSelector(state),
    exchangeLoading: exchangeUpdateLoadingSelector(state),
});

const mapDispatchToProps = {
    fetchExchanges,
    updateExchange,
    deleteExchange,
    createExchange
};

export class ExchangesTableView extends Component {
    constructor(props) {
        super(props);

        this.config = [
            {key: 'exchanges_id', width: '10%', change: false},
            {key: 'name', width: '30%'},
            {key: 'url', width: '40%'},
        ];
    }


    componentDidMount() {
        this.props.fetchExchanges();
    }

    onDeleteExchange = (exchange) => this.props.deleteExchange(exchange.exchanges_id);

    onUpdateExchange = (exchange) => this.props.updateExchange(exchange, exchange.exchanges_id);

    render() {
        const {
            exchanges,
            exchangesLoading,
            createExchange
        } = this.props;

        if (exchangesLoading || !exchanges) {
            return <div/>
        }

        return (
            <RestTable
                config={this.config}
                items={exchanges}
                deleteItem={this.onDeleteExchange}
                updateItem={this.onUpdateExchange}
                createItem={createExchange}
            />
        )
    }
}

export const ExchangesTable = connect(mapStateTpProps, mapDispatchToProps)(ExchangesTableView);