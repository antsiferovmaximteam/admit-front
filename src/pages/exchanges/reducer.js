import {combineReducers} from 'redux';

import {
    EXCHANGES_FETCH,
    EXCHANGE_UPDATE,
    EXCHANGE_DELETE,
    EXCHANGES_CREATE,

    exchangeDeleteLoadingReducer,
    exchangesFetchLoadingReducer,
    exchangeUpdateLoadingReducer,
    exchangeCreateLoadingReducer
} from './actions';

function exchangesReducer(state = [], {type, payload}) {
    switch (type) {
        case EXCHANGES_FETCH:
            return payload.data;
        case EXCHANGE_UPDATE:
            return state.map(item => item.exchanges_id !== payload.data.exchanges_id ? item : payload.data);
        case EXCHANGE_DELETE:
            return state.filter(item => item.exchanges_id !== payload.data.exchanges_id);
        case EXCHANGES_CREATE:
            return [...state, payload.data];
        default:
            return state;
    }
}

export const reducer = combineReducers({
    exchanges: exchangesReducer,
    exchangesFetchLoading: exchangesFetchLoadingReducer,
    exchangeUpdateLoading: exchangeUpdateLoadingReducer,
    exchangeDeleteLoading: exchangeDeleteLoadingReducer,
    exchangeCreateLoading: exchangeCreateLoadingReducer
});

export const exchangesSelector = state => state.exchanges.exchanges;
export const exchangesFetchLoadingSelector = state => state.exchanges.exchangesFetchLoading.isLoading;
export const exchangeUpdateLoadingSelector = state => state.exchanges.exchangeUpdateLoading.isLoading;
export const exchangeDeleteLoadingSelector = state => state.exchanges.exchangeDeleteLoading.isLoading;