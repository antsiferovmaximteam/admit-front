export {
    CategoriesView,
    CategoriesPage
} from './components/categories';
export {
    categoriesSelector,
    reducer as categoriesReducer
} from './reducer';