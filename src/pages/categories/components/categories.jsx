import React, {Component} from 'react';
import {connect} from 'react-redux';
import Modal from 'react-modal';

import {Container, Section} from 'ui';
import {fetchExchanges, exchangesSelector, exchangesFetchLoadingSelector} from 'pages/exchanges';
import {fetchCategoriesByExchange, deleteCategory, createCategory, updateCategory} from '../actions';
import {categoriesSelector} from '../reducer';
import {CategoriesEditor, CategoryForm} from '../organisms';
import {ModalHeader} from '../atoms';

const mapStateToProps = (state) => ({
    exchanges: exchangesSelector(state),
    categories: categoriesSelector(state),
    exchangedLoading: exchangesFetchLoadingSelector(state)
});

const mapDispatchToProps = {
    fetchExchanges,
    fetchCategoriesByExchange,
    deleteCategory,
    createCategory,
    updateCategory
};

const modalStyle = {
    content: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '600px',
        maxHeight: '600px',
        left: '50%',
        top: '50%',
        transform: 'translateX(-50%) translateY(-50%)'
    }
};

export class CategoriesView extends Component {
    state = {
        newCategory: null,
        editCategory: null
    };

    componentDidMount() {
        this.props.fetchExchanges();
    }

    handleExchangeSelect = (exchangeId) => {
        this.props.fetchCategoriesByExchange(exchangeId)
    };

    handleOpenCreateModal = (exchangeId, parentId) => {
        this.setState(() => ({
            newCategory:  {
                parent_category: parentId,
                exchanges_id: exchangeId
            }
        }));
    };

    handleOpenEditModal = (category) => {
        this.setState(() => ({
            editCategory: category
        }));
    };

    handleCloseCreateModal = () => this.setState(() => ({
        newCategory: undefined
    }));

    handleCloseEditModal = () => this.setState(() => ({
        editCategory: undefined
    }));

    handleCreateCategory = async (category) => {
        await this.props.createCategory({
            ...category,
            ...this.state.newCategory
        });
        this.handleCloseCreateModal();
    };

    handleEditCategory = async (category) => {
        await this.props.updateCategory(category);
        this.handleCloseEditModal();
    };

    render() {
        const {
            exchanges,
            exchangedLoading,
            categories,
            deleteCategory
        } = this.props;

        return (
            <Section>
                <Container>
                    {
                        !exchangedLoading &&
                        exchanges &&
                        <CategoriesEditor
                            exchanges={exchanges}
                            categories={categories}
                            onExchangeSelect={this.handleExchangeSelect}
                            onCreateCategory={this.handleOpenCreateModal}
                            onEditCategory={this.handleOpenEditModal}
                            onDeleteCategory={deleteCategory}
                        />
                    }
                </Container>
                <Modal
                    isOpen={!!this.state.newCategory}
                    onRequestClose={this.handleCloseCreateModal}
                    style={modalStyle}
                >
                    <ModalHeader>Create category</ModalHeader>
                    <CategoryForm onSubmit={this.handleCreateCategory}/>
                </Modal>
                <Modal
                    isOpen={!!this.state.editCategory}
                    onRequestClose={this.handleCloseEditModal}
                    style={modalStyle}
                >
                    <ModalHeader>Edit category</ModalHeader>
                    <CategoryForm onSubmit={this.handleEditCategory} category={this.state.editCategory}/>
                </Modal>
            </Section>
        )
    }
}

export const CategoriesPage = connect(mapStateToProps, mapDispatchToProps)(CategoriesView);