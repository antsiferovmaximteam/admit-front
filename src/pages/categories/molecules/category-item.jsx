import React from "react";
import styled from 'styled-components';
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {Row, FlatButton} from '../atoms';

const EditButton = styled(FlatButton)`
  margin-left: auto;
`;

const CategoryRow = styled(Row)`
  ${EditButton}, ${FlatButton} {
      opacity: 0;
  }

  &:hover {
    ${EditButton}, ${FlatButton} {
      opacity: 1;
    }
  }
`;

export const CategoryItem = ({children, onDelete, onEdit, ...props,}) => {
    const handleDeleteClick = e => {
        e.stopPropagation();
        onDelete(props['data-id'], props['data-index']);
    };

    const handleEditClick = e => {
        e.stopPropagation();
        onEdit(props['data-id'])
    };

    return (
        <CategoryRow {...props}>
            {children}
            <EditButton onClick={handleEditClick}>
                <Icon icon="pen"/>
            </EditButton>
            <FlatButton onClick={handleDeleteClick}>
                <Icon icon="trash-alt"/>
            </FlatButton>
        </CategoryRow>
    )
};