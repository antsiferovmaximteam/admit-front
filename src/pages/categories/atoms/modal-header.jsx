import styled from 'styled-components';

export const ModalHeader = styled('h3')`
  font-size: 1.5rem;
  font-weight: bold;
  letter-spacing: 3px;
  text-align: center;
`;