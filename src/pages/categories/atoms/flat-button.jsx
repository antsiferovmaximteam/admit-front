import styled from "styled-components";

export const FlatButton = styled('button')`
  background: none;
  border: none;
  cursor: pointer;
  color: inherit;
`;
