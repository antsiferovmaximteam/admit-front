import styled from "styled-components";
import {colors} from "ui/theme";

const css = String.raw;

export const Row = styled('li')`
  display: flex;
  justify-content: space-between;
  padding: 10px 10px;
  margin-bottom: 10px;
  border: 1px solid ${colors.dividerBorder};
  border-radius: 3px;
  cursor: pointer;
  font-size: .85rem;
  
  ${p => p.active && css`
    background-color: black;
    border-color: black;
    color: white;
  `}
`;
