import styled from "styled-components";
import {colors} from "../../../ui/theme";

export const Column = styled('ul')`
  min-width: 250px;
  padding: 0 30px 100px 30px;
  margin: 0;
  border-right: 1px solid ${colors.dividerBorder};
  list-style: none;
  
  &:first-child {
    padding-left: 0;
  }
`;