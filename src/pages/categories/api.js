import {axiosInstant} from 'lib/net';

export class Api {
    static async getCategoriesByExchange(id) {
        const res = await axiosInstant.get(`/categories?exchanges_id=${id}`);
        return res.data;
    }

    static async deleteCategory(id) {
        const res = await axiosInstant.delete(`/categories/${id}`);
        return res.data;
    }

    static async createCategory(category) {
        const res = await axiosInstant.post('/categories', category);
        return res.data;
    }

    static async updateCategory(category, id) {
        const res = await axiosInstant.put(`/categories/${id}`, category);
        return res.data;
    }
}