import {
    CATEGORIES_FETCH,
    CATEGORIES_DELETE,
    CATEGORIES_CREATE,
    CATEGORIES_UPDATE
} from './actions';

function categoriesReducer(state = [], {type, payload}) {
    switch (type) {
        case CATEGORIES_FETCH:
            return payload.data;
        case CATEGORIES_DELETE:
            return state.filter(item => item.categories_id !== payload);
        case CATEGORIES_CREATE:
            return [...state, payload];
        case CATEGORIES_UPDATE:
            return state.map(
                category => category.categories_id !== payload.categories_id ? category : payload
            );
        default:
            return state;
    }
}

export const reducer = categoriesReducer;

export function categoriesSelector(state) {
    return state.categories
}