import {Api} from './api';

export const CATEGORIES_FETCH = 'CATEGORIES_FETCH';
export const CATEGORIES_DELETE = 'CATEGORIES_DELETE';
export const CATEGORIES_CREATE = 'CATEGORIES_CREATE';
export const CATEGORIES_UPDATE = 'CATEGORIES_UPDATE';

export const fetchCategoriesByExchange = (id) => async dispatch => {
    const categories = await Api.getCategoriesByExchange(id);

    dispatch({
        type: CATEGORIES_FETCH,
        payload: categories
    })
};

export const deleteCategory = (id) => async dispatch => {
    await Api.deleteCategory(id);

    dispatch({
        type: CATEGORIES_DELETE,
        payload: id
    });
};

export const createCategory = (category) => async dispatch => {
    const res = await Api.createCategory(category);

    dispatch({
        type: CATEGORIES_CREATE,
        payload: res.data
    });
};

export const updateCategory = (category) => async dispatch => {
    console.log(category);
    const res = await Api.updateCategory(category, category.categories_id);

    dispatch({
        type: CATEGORIES_UPDATE,
        payload: res.data
    })
};