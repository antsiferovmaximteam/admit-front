import React, {Component} from 'react';
import styled from 'styled-components';

import {Breadcrumbs} from 'ui';
import {CategoryItem} from '../molecules';
import {Row, Column} from '../atoms';

const Section = styled('section')`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  padding: 10px 0;
  overflow-x: scroll;
`;

const ButtonAddCategory = styled('button')`
  padding: 6px 10px;
  font-weight: 600;
  font-size: .9rem;
  background: none;
  border: none;
  text-decoration: underline;
  cursor: pointer;
`;

export class CategoriesEditor extends Component {
    state = {
        categoriesPath: [],
        activeExchangeId: null
    };

    replace = (item) => {
        const temp = [];

        for (let element of this.state.categoriesPath) {
            if (element.index === item.index) {
                temp.push(item);
                this.setState({categoriesPath: temp});
                return;
            }

            temp.push(element);
        }

        temp.push(item);
        this.setState({categoriesPath: temp});
    };

    remove = (index) => {
        const temp = [];

        for (let element of this.state.categoriesPath) {
            if (element.index === index) {
                this.setState({categoriesPath: temp});
                return;
            }

            temp.push(element);
        }

        this.setState({categoriesPath: temp});
    };

    handleCategoryClick = (e) => {
        const id = +e.target.dataset.id;
        const index = +e.target.dataset.index;
        const category = {
            ...this.props.categories.find(item => item.categories_id === id),
            index
        };

        this.replace(category, item => item.index === index);
    };

    handleExchangeClick = (e) => {
        const id = +e.target.dataset.id;

        this.props.onExchangeSelect(id);
        this.setState({
            categoriesPath: [],
            activeExchangeId: id
        });
    };

    handleDelete = async (id, index) => {
        await this.props.onDeleteCategory(id);

        this.remove(index)
    };

    handleEdit = async id => {
        const {categories, onEditCategory} = this.props;
        const category = categories.filter(item => item.categories_id === id)[0];

        onEditCategory(category)
    };

    renderAddCategoryButton = (title, parentId) => (
        <li>
            <ButtonAddCategory
                onClick={() => this.props.onCreateCategory(this.state.activeExchangeId, parentId)}
            >
                {title}
            </ButtonAddCategory>
        </li>
    );

    renderCategories = (categories, index, parentId) => categories.map(category => (
        <CategoryItem
            active={parentId === category.categories_id}
            data-index={index}
            data-id={category.categories_id}
            onDelete={this.handleDelete}
            onEdit={this.handleEdit}
            onClick={this.handleCategoryClick}
            key={category.categories_id}
        >
            {category.name}
        </CategoryItem>
    ));

    renderNestedCategories = () => this.state.categoriesPath
        .map((parent, index, parentIds) => {
            const categoriesByParent = this.props.categories
                .filter(category => category.parent_category === parent.categories_id);

            const parentId = parentIds[index + 1] ? parentIds[index + 1].categories_id : undefined;

            return (
                <Column key={parent.categories_id}>
                    {this.renderCategories(categoriesByParent, index, parentId)}
                    {this.renderAddCategoryButton('+ New sub-category', parent.categories_id)}
                </Column>
            )
        });

    renderRootCategories = () => {
        const {categories} = this.props;

        const categoriesWithoutParent = categories.filter(category => !category.parent_category);
        const activeCategory = this.state.categoriesPath[0];
        const activeId = activeCategory ? activeCategory.categories_id : null;

        return (
            <Column key="categories-without-parent">
                {this.renderCategories(categoriesWithoutParent, -1, activeId)}
                {this.renderAddCategoryButton('+ New category')}
            </Column>
        );
    };

    renderBreadcrumbs = (exchanges, activeExchangeId) => {
        const activeExchange = exchanges.find(exchange => exchange.exchanges_id === activeExchangeId);

        return (
            <Breadcrumbs>
                {activeExchange && (
                    <Breadcrumbs.Item
                        onClick={this.handleExchangeClick}
                        data-id={activeExchange.exchanges_id}
                    >
                        {activeExchange.name}
                    </Breadcrumbs.Item>
                )}
                {this.state.categoriesPath.map(item => (
                    <Breadcrumbs.Item
                        key={item.categories_id}
                        data-id={item.categories_id}
                        data-index={item.index}
                        onClick={this.handleCategoryClick}
                    >
                        {item.name}
                    </Breadcrumbs.Item>
                ))}
            </Breadcrumbs>
        )
    };

    render() {
        const {exchanges} = this.props;
        const {activeExchangeId} = this.state;

        return (
            <>
                {this.renderBreadcrumbs(exchanges, activeExchangeId)}
                <Section>
                    <Column>
                        {exchanges.map(item => (
                            <Row
                                active={this.state.activeExchangeId === item.exchanges_id}
                                onClick={this.handleExchangeClick}
                                data-id={item.exchanges_id}
                                key={item.exchanges_id}
                            >{item.name}</Row>
                        ))}
                    </Column>
                    {activeExchangeId && (
                        <>
                            {this.renderRootCategories()}
                            {this.renderNestedCategories()}
                        </>
                    )}
                </Section>
            </>
        );
    }
}