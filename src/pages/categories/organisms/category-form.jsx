import React, {useState} from 'react';
import styled from 'styled-components';
import {TextInput} from "../../../ui/molecules";

const Form = styled('form')`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin-top: 50px;
  height: 100%;
`;

const InputField = styled('div')`
  margin: 15px 0;
`;

const CreateButton = styled('button')`
  display: block;
  padding: 5px 15px;
  margin: auto auto 50px;
  border: 1px solid black;
  box-shadow: none;
  color: black;
  cursor: pointer;
`;

export const CategoryForm = ({onSubmit, category = {}}) => {
    const [name, setName] = useState(category.name || '');
    const [url, setUrl] = useState(category.url || '');

    const handleSubmit = e => {
        e.preventDefault();

        onSubmit({
            ...category,
            name, url
        });
    };

    return (
        <Form onSubmit={handleSubmit}>
            <InputField>
                <TextInput
                    placeholder="Markup"
                    name="name"
                    onChange={setName}
                    value={name}
                >Name</TextInput>
            </InputField>
            <InputField>
                <TextInput
                    placeholder="/category/sub-category"
                    name="url"
                    onChange={setUrl}
                    value={url}
                >Url</TextInput>
            </InputField>
            <CreateButton
                type="submit"
            >
                Create
            </CreateButton>
        </Form>
    )
};