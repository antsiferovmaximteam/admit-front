import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';

import {Section, Container, Tabs, RestTable} from 'ui';
import {fetchExchanges, exchangesSelector} from 'pages/exchanges';
import {fetchTargets, targetsSelector} from 'pages/targets';
import {
    fetchOrdersConfigByExchanges,
    updateOrderConfig,
    createOrderConfig
} from 'pages/orders-configs/actions';
import {ordersConfigsSelector} from 'pages/orders-configs/reducer';
import {TableContainer} from './table-container';

const mapStateToProps = state => ({
    exchanges: exchangesSelector(state),
    targets: targetsSelector(state),
    ordersConfigs: ordersConfigsSelector(state)
});

const mapDispatchToProps = {
    fetchExchanges,
    fetchTargets,
    createOrderConfig,
    updateOrderConfig,
    fetchOrdersConfigByExchanges
};

export const OrdersConfigView = (
    {
        exchanges,
        ordersConfigs,
        targets,
        fetchExchanges,
        createOrderConfig,
        updateOrderConfig,
        fetchOrdersConfigByExchanges,
        fetchTargets
    }
) => {
    const [initialized, setInitialized] = useState(false);
    const [activeTab, setActiveTab] = useState({});
    useEffect(() => {
        if (!initialized) {
            fetchExchanges().then(() => {
                setInitialized(true);
            });
        } else {
            if (!activeTab.exchanges_id) {
                setActiveTab(exchanges[0]);
                fetchOrdersConfigByExchanges(exchanges[0].exchanges_id);
                fetchTargets();
            }
        }
    });

    const selectExchange = (exchange) => {
        fetchOrdersConfigByExchanges(exchange.exchanges_id);
        setActiveTab(exchange);
    };

    const createOrdersConfigs = (orderConfig) => {
        const {targets_id} = targets.find(item => item.name === orderConfig.target_name);

        const updatedOrderConfig = {
            ...orderConfig,
            exchanges_id: activeTab.exchanges_id,
            targets_id
        };

        createOrderConfig(updatedOrderConfig);
    };

    const updateOrdersConfigs = (orderConfig) => {
        const {targets_id} = targets.find(item => item.name === orderConfig.target_name);
        const oldOrderConfig = ordersConfigs.find(item => item.orders_config_id === orderConfig.orders_config_id);

        const newOrderConfig = {
            ...oldOrderConfig,
            ...orderConfig,
            exchanges_id: activeTab.exchanges_id,
            targets_id
        };

        updateOrderConfig(newOrderConfig);
    };

    const options = targets.map(target => ({value: target.name, title: target.name}));

    const config = [
        {key: 'orders_config_id', width: '15%', change: false},
        {key: 'target_name', width: '15%', selected: true, options},
        {key: 'query', width: '55%'}
    ];

    return (
        <Section>
            <Container>
                <Tabs>
                    {exchanges.map(exchange => (
                        <Tabs.Item
                            key={exchange.exchanges_id}
                            active={exchange.exchanges_id === activeTab.exchanges_id}
                            onClick={() => selectExchange(exchange)}
                        >
                            {exchange.name}
                        </Tabs.Item>
                    ))}
                </Tabs>
                <TableContainer>
                    <RestTable
                        config={config}
                        items={ordersConfigs}
                        createItem={createOrdersConfigs}
                        updateItem={updateOrdersConfigs}
                    />
                </TableContainer>
            </Container>
        </Section>
    )

};

export const OrdersConfigPage = connect(
    mapStateToProps,
    mapDispatchToProps
)(OrdersConfigView);