import {axiosInstant} from 'lib/net';

export class Api {
    static async getByExchange(id) {
        const res = await axiosInstant.get(`/orders-configs?exchanges_id=${id}`);
        return res.data;
    }

    static async createOrderConfig(orderConfig) {
        const res = await axiosInstant.post(`/orders-configs`, orderConfig);
        return res.data;
    }

    static async updateOrderConfig(orderConfig, id) {
        const res = await axiosInstant.put(`/orders-configs/${id}`, orderConfig);
        return res.data;
    }
}