import {Api} from './api';

export const FETCH_ORDERS_CONFIGS = 'FETCH_ORDERS_CONFIGS';
export const UPDATE_ORDERS_CONFIGS = 'UPDATE_ORDERS_CONFIGS';
export const CREATE_ORDERS_CONFIGS = 'CREATE_ORDERS_CONFIGS';

export const fetchOrdersConfigByExchanges = (exchangesId) => async dispatch => {
    const res = await Api.getByExchange(exchangesId);

    dispatch({
        type: FETCH_ORDERS_CONFIGS,
        payload: res.data
    });
};

export const createOrderConfig = orderConfig => async dispatch => {
    const res = await Api.createOrderConfig(orderConfig);

    dispatch({
        type: CREATE_ORDERS_CONFIGS,
        payload: res.data
    });
};

export const updateOrderConfig = orderConfig => async dispatch => {
    const res = await Api.updateOrderConfig(orderConfig, orderConfig.orders_config_id);

    dispatch({
        type: UPDATE_ORDERS_CONFIGS,
        payload: res.data
    })
};