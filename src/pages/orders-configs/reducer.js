import {
    FETCH_ORDERS_CONFIGS,
    UPDATE_ORDERS_CONFIGS,
    CREATE_ORDERS_CONFIGS
} from './actions';

export function orderConfigsReducer(state = [], {type, payload}) {
    switch (type) {
        case FETCH_ORDERS_CONFIGS: return payload;
        case CREATE_ORDERS_CONFIGS: return [...state, payload];
        case UPDATE_ORDERS_CONFIGS: return state.map(item => item.orders_config_id !== payload.orders_config_id ? item : payload)
        default: return state;
    }
}

export const ordersConfigsSelector = state => state.ordersConfigs;