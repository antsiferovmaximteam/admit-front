import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL || '';
const config = { baseURL: API_URL };

export const axiosInstant = axios.create(config);

axiosInstant.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    error.message = error.response.data.message;

    return Promise.reject(error);
});