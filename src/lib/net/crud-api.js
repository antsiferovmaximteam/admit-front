import {axiosInstant} from 'lib/net';

export class CRUDApi {
    endpoint = null;

    constructor(url) {
        this.endpoint = url;
    }

    fetch = async () => {
        const res = await axiosInstant.get(this.endpoint);
        return res.data;
    };

    create = async (payload) => {
        const res = await axiosInstant.post(this.endpoint, payload);
        return res.data;
    };

    update = async (payload, id) => {
        const res = await axiosInstant.put(`${this.endpoint}/${id}`, payload);
        return res.data;
    };

    delete = async (id) => {
        const res = await axiosInstant.delete(`${this.endpoint}/${id}`);
        return res.data;
    }
}