export const CRUDAction = (request, loading, notify) => (...params) => async dispatch => {
    try {
        dispatch(loading.fetch());

        const res = await request(...params);
        dispatch(loading.finish({
            ...res,
            notify
        }));
    } catch (e) {
        dispatch(loading.failure({
            message: e.message,
            notify
        }))
    }
};