import {notify as reapopNotify} from 'reapop';

const failureMessage = (message) => ({
    title: 'Failure',
    message: message,
    status: 'error',
    dismissible: true,
    dismissAfter: 3000
});

const successMessage = (message) => ({
    title: 'Success',
    message: message,
    status: 'success',
    dismissible: true,
    dismissAfter: 2000
});

export function makeLoadingReducer(name) {
    const FETCH_START = name + '_START';
    const FETCH_FINISH = name;
    const FETCH_FAILURE = name + '_FAILURE';

    return {
        constants: {
            FETCH: FETCH_START,
            FETCH_FINISH,
            FETCH_FAILURE
        },
        actions: {
            fetch: (payload) => {
                return {
                    type: FETCH_START,
                    payload
                }
            },
            finish: (payload = {}) => dispatch => {
                payload.notify && dispatch(reapopNotify(successMessage(payload.message)));

                dispatch({
                    type: FETCH_FINISH,
                    payload
                });

                return Promise.resolve();
            },
            failure: (payload = {}) => dispatch => {
                payload.notify && dispatch(reapopNotify(failureMessage(payload.message)));

                dispatch({
                    type: FETCH_FAILURE,
                    payload
                });

                return Promise.resolve()
            }
        },
        reducer: function loadingReducer(state = {isLoading: false}, {type, payload}) {
            switch (type) {
                case FETCH_START:
                    return {
                        ...state,
                        isLoading: true
                    };
                case FETCH_FINISH:
                    return {
                        ...state,
                        isLoading: false
                    };
                case FETCH_FAILURE:
                    return {
                        ...state,
                        isLoading: false,
                        message: payload
                    };
                default: return state;
            }
        }
    }
}