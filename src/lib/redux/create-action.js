export function createAction(name, payload) {
    return {
        type: name,
        payload
    }
}