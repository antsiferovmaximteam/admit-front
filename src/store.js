import {applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import {reducer} from './reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore(initialState = {}) {
    const middlewares = [
        thunk
    ];

    if (process.env.NODE_ENV === 'development') {
        middlewares.push(createLogger({ collapsed: true }));
    }

    return createStore(
        reducer,
        initialState,
        composeEnhancers(applyMiddleware(...middlewares))
    );
}