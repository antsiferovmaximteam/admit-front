import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import {createGlobalStyle} from 'styled-components';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Provider as ReduxProvider} from 'react-redux';
import NotificationsSystem from 'reapop';
import reapopTheme from 'reapop-theme-wybo';
import Modal from 'react-modal';

import {App} from './app';
import {globalStyles} from './ui/theme';
import * as serviceWorker from './lib/service-worker';
import {configureStore} from 'store';

const GlobalStyles = createGlobalStyle`${globalStyles}`;
const store = configureStore({});

Modal.setAppElement('#root');

ReactDOM.render(
    <Fragment>
        <ReduxProvider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={App}/>
                </Switch>
            </BrowserRouter>
            <NotificationsSystem theme={reapopTheme}/>
        </ReduxProvider>
        <GlobalStyles/>
    </Fragment>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
