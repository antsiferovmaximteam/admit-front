import {combineReducers} from "redux";
import {reducer as notificationsReducer} from 'reapop';

import {exchangesReducer as exchanges} from "./pages/exchanges";
import {targetsReducer as targets} from "./pages/targets";
import {categoriesReducer as categories} from "./pages/categories";
import {orderConfigsReducer as ordersConfigs} from './pages/orders-configs';

export const reducer = combineReducers({
    exchanges,
    targets,
    categories,
    ordersConfigs,
    notifications: notificationsReducer()
});