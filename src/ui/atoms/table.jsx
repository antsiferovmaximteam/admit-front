import styled from 'styled-components';
import {colors} from 'ui/theme';

export const Table = styled('table')`
  width: 100%;
  border-collapse: collapse; 
`;

export const TableRow = styled('tr')`

`;

export const TableColumn = styled('td')`
  padding: 10px;
`;

export const TableHead = styled('thead')`
  font-weight: bold;

  ${TableRow} {
    border-top: 1px solid ${colors.dividerBorder};
    border-bottom: 2px solid ${colors.dividerBorder};
  }
`;

export const TableBody = styled('tbody')`
  ${TableRow} {
    border-bottom: 1px solid ${colors.dividerBorder};
  }
`;