import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled('div')`
  display: flex;
  max-width: 1200px;
  margin: 0 auto;
  padding: 0 15px;
  
  flex-direction: ${p => p.dir};
  justify-content: ${p => p.justify};
  align-items: ${p => p.align};
`;

export const Container = ({children, dir, justify, align}) => (
    <StyledContainer dir={dir} justify={justify} align={align}>
        {children}
    </StyledContainer>
);

Container.defaultProps = {
    dir: 'column',
    justify: 'space-between',
    align: 'stretch'
};