import styled from "styled-components";
import {colors} from 'ui/theme';

export const TableInput = styled('input')`
  width: 100%;
  background-color: transparent;
  box-shadow: none;
  margin-bottom: -1px;
  border: 1px solid #dee2e6;
  outline: none;
  transition: .2s all;
  border-radius: 3px;
  padding: 5px;
  
  &:focus {
    border-color: ${colors.primary};
  }
`;