import React from 'react';
import styled, {ThemeProvider} from 'styled-components';
import PropTypes from 'prop-types';

import {theme as GlobalTheme} from 'ui/theme';

const theme = GlobalTheme.button;
const css = String.raw;

const StyledButton = styled('button')`
  border: 1px solid;
  border-radius: 3px;
  cursor: pointer;
  transition: all .2s;
  
  color: ${p => p.theme.color};
  background-color: ${p => p.theme.backgroundColor};
  border-color: ${p => p.theme.borderColor};
  
  padding: ${p => p.theme.padding};
  font-size: ${p => p.theme.fontSize};
  
  &:hover {
    background-color: ${p => p.theme.backgroundColorHover};
    border-color: ${p => p.theme.borderColorHover};
  }
  
  ${p => p.outline && css`
    color: ${p.theme.borderColor};
    background-color: transparent;
   
    &:hover {
      color: ${p.theme.color};
      background-color: ${p => p.theme.backgroundColor};
    }
  `}
`;

export const Button = ({variant, size, children, ...props}) => (
   <ThemeProvider theme={{
       ...theme[variant],
       ...theme[size]
   }}>
       <StyledButton {...props}>
           {children}
       </StyledButton>
   </ThemeProvider>
);

Button.defaultProps = {
    variant: 'primary',
    size: 'medium',
};

Button.propTypes = {
    variant: PropTypes.oneOf(['primary', 'red', 'yellow', 'green']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
};