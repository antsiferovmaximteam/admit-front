import React from 'react';
import styled from 'styled-components';

const Item = styled('li')`
  padding: 5px 15px;
  cursor: pointer;
  
  &:first-child {
    padding-left: 0;
  }
`;

const BreadcrumbsContainer = styled('ul')`
  display: flex;
  list-style: none;
  padding: 15px 0;
  margin: 0;
`;

export const Breadcrumbs = ({children}) => (
    <BreadcrumbsContainer>
        {children}
    </BreadcrumbsContainer>
);

const BreadcrumbItem = ({children, onClick, ...props}) => (
    <Item onClick={onClick} {...props}>
        {children}
    </Item>
);

Breadcrumbs.Item = BreadcrumbItem;