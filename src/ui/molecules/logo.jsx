import React from 'react';
import styled from 'styled-components';

const Link = styled('a')`
  display: block;
  width: 100%;
`;

const Image = styled('img')`
  width: 100%;
`;

export const Logo = ({href, img, alt}) => (
    <Link href={href}>
        <Image src={img} alt={alt}/>
    </Link>
);

Logo.defaultProps = {
    href: '#',
    alt: 'Logo'
};