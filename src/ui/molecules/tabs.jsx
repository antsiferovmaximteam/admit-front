import React from 'react';
import styled from 'styled-components';

const css = String.raw;

export const Tabs = styled('ul')`
  display: flex;
  list-style: none;
  padding: 0;
  margin: 0;
`;

const TabItem = styled('li')`
  padding: 10px 20px;
  border: 1px solid black;
  border-right-color: transparent;
  margin-right: -1px;
  
  ${p => p.active && css`
    color: white;
    background-color: black;
  `}
  
  &:first-child {
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  
  &:last-child {
    border-right-color: black;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
  }
`;

Tabs.Item = TabItem;