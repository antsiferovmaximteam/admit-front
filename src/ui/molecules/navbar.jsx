import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

const StyledNavbar = styled('ul')`
  display: flex;
  list-style: none;
  padding: 0;
  margin: 0;
`;

const StyledNavbarItem = styled('li')`
  a {
    display: block;
    color: black;
    padding: 25px 15px;
    
    &:hover {
      color: #343434;
    }
  }
`;

export const Navbar = ({children}) => (
    <StyledNavbar>
        {children}
    </StyledNavbar>
);

Navbar.Item = ({children, to}) => (
    <StyledNavbarItem>
        <Link to={to}>
            {children}
        </Link>
    </StyledNavbarItem>
);