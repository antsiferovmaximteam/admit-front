import React, {useRef} from 'react';
import styled from 'styled-components';
import {colors} from "../theme";

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;

const Label = styled('label')`
  padding-left: 10px;
  font-size: .9rem;
  font-weight: 600;
`;

const Input = styled('input')`
  margin-top: 3px;
  padding: 7px 10px;
  border-radius: 2px;
  border: none;
  border-bottom: 2px solid #c7c7c7;
  box-shadow: none;
  outline: none;
  
  &:focus {
    border-bottom-color: ${colors.primary};
  }
`;

export const TextInput = ({children, placeholder, name, onChange, value}) => {
    const inputRef = useRef(null);

    const handleChange = () => onChange(inputRef.current.value, name);

    return (
        <Wrapper>
            <Label htmlFor={name}>{children}</Label>
            <Input
                ref={inputRef}
                placeholder={placeholder}
                id={name}
                name={name}
                value={value}
                onChange={handleChange}
            />
        </Wrapper>
    )
};