import {normalize} from "styled-normalize";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSave, faTrashAlt, faPen, faPlus } from '@fortawesome/free-solid-svg-icons'
export {theme} from './theme';
export {colors} from './colors';

library.add(
    faSave,
    faTrashAlt,
    faPen,
    faPlus
);

const css = String.raw;
export const font = {
    main: 'Lato, sans-serif'
};
export const globalStyles = css`
  ${normalize}

  html {
    font-size: 100%;
    font-family: ${font.main};
    -webkit-font-smoothing: antialiased;
    margin: 0;
    padding: 0;
    background-color: #F8F9FD;
  }
  
  a {
    color: inherit;
    text-decoration: none;
    
    &:hover {
      color: inherit;
      text-decoration: none;
    }
  }
  
  * {
    box-sizing: border-box;
  }
  
  h1, h2, h3, h4 {
    margin: 0;
  }
  
  ul, li {
    margin: 0;
    padding: 0;
  }
`;