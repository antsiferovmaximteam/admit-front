import {colors} from './colors';

export const theme = {
    button: {
        primary: {
            color: '#fff',
            backgroundColor: colors.primary,
            borderColor: colors.primary,
            backgroundColorHover: '#6848a0',
            borderColorHover: '#6848a0'
        },
        red: {
            color: '#fff',
            backgroundColor: '#dc3545',
            borderColor: '#dc3545',
            backgroundColorHover: '#d23544',
            borderColorHover: '#d23544'
        },
        yellow: {
            color: '#212529',
            backgroundColor: '#ffc107',
            borderColor: '#ffc107',
            backgroundColorHover: '#f5b707',
            borderColorHover: '#f5b707'
        },
        green: {
            color: '#fff',
            backgroundColor: '#28a745',
            borderColor: '#28a745',
            backgroundColorHover: '#289D44',
            borderColorHover: '#289D44'
        },
        small: {
            padding: '.25rem .5rem',
            fontSize: '.7rem'
        },
        medium: {
            padding: '.375rem .75rem',
            fontSize: '1rem'
        },
        large: {
            padding: '.5rem 1rem',
            fontSize: '1.3rem'
        }
    }
};