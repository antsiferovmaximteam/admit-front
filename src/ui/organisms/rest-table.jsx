import React, {Component} from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types';

import {Table, TableBody, TableColumn as Col, TableRow as Row, TableHead, Button, TableInput} from 'ui';
import {capitalize} from 'lib/strings';
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";

const TableButton = styled(Button)`
  margin: 0 10px;
`;

export class RestTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editable: {
                index: null,
                data: null
            },
            newItem: this.generateNewItemDefaultData(this.getConfig())
        };
    }

    generateNewItemDefaultData(config) {
        return config.reduce((acc, item) => (item.change ? acc[item.key] = '' : acc), {});
    }

    formatKeyToName(key) {
        return capitalize(key.replace(/_/g, ' '));
    }

    getConfig = () => this.props.config.map(item => item.name ? item : {...item, name: this.formatKeyToName(item.key)});

    handleEditableChange = (name, value) => this.setState(state => ({
        editable: {
            ...state.editable,
            data: {
                ...state.editable.data,
                [name]: value
            }
        }
    }));

    handleNewChange = (name, value) => this.setState(state => ({
        newItem: {
            ...state.newItem,
            [name]: value
        }
    }));

    handleEditClick = (index) => this.setState(() => ({
        editable: {
            index,
            data: this.props.items[index]
        }
    }));

    handleCreateClick = () => {
        this.props.createItem(this.state.newItem);
        this.setState(() => ({
            newItem: this.generateNewItemDefaultData(this.getConfig())
        }));
    };

    handleSaveEditClick = async () => {
        this.props.updateItem(this.state.editable.data);
        this.setState(() => ({
            editable: {
                index: null,
                data: null
            }
        }));
    };

    handleDeleteClick = (index) => {
        this.props.deleteItem(this.props.items[index]);
    };

    renderEditableCol = (confItem, item, onChange) => {
        const value = item[confItem.key] || '';

        if (confItem.change === false) {
            return value;
        }

        if (confItem.selected) {
            return (
                <select value={value} onChange={e => onChange(confItem.key, e.target.value)}>
                    {confItem.options.map(item => (
                        <option
                            value={item.value}
                            key={item.value}
                        >
                            {item.title}
                        </option>
                    ))}
                </select>
            )
        }

        const ref = React.createRef();

        return (
            <TableInput type="text" ref={ref} value={value} onChange={() => onChange(confItem.key, ref.current.value)}/>
        )
    };

    renderCreateRow = () => (
        <Row>
            {this.getConfig().map((confItem, index) => (
                <Col key={confItem.key + index}>
                    {this.renderEditableCol(confItem, this.state.newItem, this.handleNewChange)}
                </Col>
            ))}
            <Col>
                <TableButton
                    variant="green"
                    onClick={this.handleCreateClick}
                >
                    <Icon icon='plus'/>
                </TableButton>
            </Col>
        </Row>
    );

    renderEditableRow = (item, index) => (
        <Row key={index}>
            {this.getConfig().map((confItem, index) => (
                <Col key={confItem.key + index}>
                    {this.renderEditableCol(confItem, this.state.editable.data, this.handleEditableChange)}
                </Col>
            ))}
            <Col>
                <TableButton
                    variant="green"
                    onClick={this.handleSaveEditClick}
                >
                    <Icon icon='save'/>
                </TableButton>
            </Col>
        </Row>
    );

    renderPureRow = (item, index) => (
        <Row key={index}>
            {this.getConfig().map(confItem => (
                <Col key={index + confItem.key}>{item[confItem.key]}</Col>
            ))}
            <Col>
                <TableButton
                    onClick={() => this.handleEditClick(index)}
                >
                    <Icon icon="pen"/>
                </TableButton>
                <TableButton
                    variant="red"
                    onClick={() => this.handleDeleteClick(index)}
                >
                    <Icon icon="trash-alt"/>
                </TableButton>
            </Col>
        </Row>
    );

    render() {
        const config = this.getConfig();
        const {items} = this.props;
        const {editable} = this.state;

        return (
            <Table>
                <TableHead>
                    <Row>
                        {config.map(item => (
                            <Col key={item.key} width={item.width}>{item.name}</Col>
                        ))}
                        <Col/>
                    </Row>
                </TableHead>
                <TableBody>
                    {
                        items.map((item, index) => editable.index === index ?
                            this.renderEditableRow(item, index) :
                            this.renderPureRow(item, index))
                    }
                    {this.renderCreateRow()}
                </TableBody>
            </Table>
        )
    }
}

RestTable.propTypes = {
    items: PropTypes.array,
    config: PropTypes.array,
    createItem: PropTypes.func,
    updateItem: PropTypes.func,
    deleteItem: PropTypes.func,
    isCreateItem: PropTypes.bool
};