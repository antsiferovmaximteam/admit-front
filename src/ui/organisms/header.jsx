import React from 'react';
import styled from 'styled-components';

import logoImg from 'assets/images/diagram.svg';
import {Container, Logo, Navbar} from 'ui';
import {routes} from 'routes';

const StyledHeader = styled('header')`
  box-shadow: 0px -10px 29px 0px #000000;
`;

const LogoWrapper = styled('div')`
  width: 40px
`;

const NavbarWrapper = styled('nav')``;

export const Header = () => (
    <StyledHeader>
        <Container dir="row" align="center">
            <LogoWrapper>
                <Logo img={logoImg}/>
            </LogoWrapper>
            <NavbarWrapper>
                <Navbar>
                    {Object.keys(routes).map((key, index) => (
                        <Navbar.Item
                            to={routes[key].path}
                            key={index}
                        >
                            {routes[key].name}
                        </Navbar.Item>
                    ))}
                </Navbar>
            </NavbarWrapper>
        </Container>
    </StyledHeader>
);