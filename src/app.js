import React, {Component, Fragment} from 'react';

import {Header} from 'ui';
import {Routes} from 'routes';

export class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <Routes/>
            </Fragment>
        );
    }
}