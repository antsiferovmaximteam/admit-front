import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import {ExchangesPage} from 'pages/exchanges';
import {CategoriesPage} from 'pages/categories';
import {TargetsPage} from 'pages/targets';
import {OrdersConfigPage} from 'pages/orders-configs';

export const routes = {
    exchanges: {
        path: '/exchanges',
        name: 'Exchanges'
    },
    categories: {
        path: '/categories',
        name: 'Categories'
    },
    targets: {
        path: '/targets',
        name: 'Targets'
    },
    ordersConfig: {
        path: '/orders-config',
        name: 'Orders Config'
    }
};

export const Routes = () => (
    <Switch>
        <Route path={routes.exchanges.path} component={ExchangesPage}/>
        <Route path={routes.categories.path} component={CategoriesPage}/>
        <Route path={routes.targets.path} component={TargetsPage}/>
        <Route path={routes.ordersConfig.path} component={OrdersConfigPage}/>
        <Redirect to="/exchanges"/>
    </Switch>
);